## Snake
_By Karol Telus_

This is the Snake game that has been written basing on the JAVA 8 and Swing technology. In this application the following design patterns are used:

- Observer (ActionListener)
- Chain of Responsibility
- Factory Method