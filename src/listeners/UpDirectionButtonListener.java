package listeners;

import board.Board;
import board.Direction;
import board.DirectionResolver;

import javax.swing.*;

public class UpDirectionButtonListener extends DirectionButtonListener {

    public UpDirectionButtonListener(JTextPane textPane, DirectionResolver directionResolver, Board board) {
        super(textPane, directionResolver, board, Direction.UP);
    }

}
