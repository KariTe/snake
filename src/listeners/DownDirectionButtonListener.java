package listeners;

import board.Board;
import board.Direction;
import board.DirectionResolver;

import javax.swing.*;

public class DownDirectionButtonListener extends DirectionButtonListener {

    public DownDirectionButtonListener(JTextPane textPane, DirectionResolver directionResolver, Board board) {
        super(textPane, directionResolver, board, Direction.DOWN);
    }

}
