package listeners;

import board.Board;
import board.Direction;
import board.DirectionResolver;

import javax.swing.*;

public class RightDirectionButtonListener extends DirectionButtonListener {

	public RightDirectionButtonListener(JTextPane textPane, DirectionResolver directionResolver, Board board) {
		super(textPane, directionResolver, board, Direction.RIGHT);
	}

}
