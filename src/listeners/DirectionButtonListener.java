package listeners;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextPane;

import board.Board;
import board.Direction;
import board.DirectionResolver;

public abstract class DirectionButtonListener implements ActionListener {
	
	private final JTextPane textPane;
	private final DirectionResolver directionResolver;
	private final Board board;
	private final Direction direction;

	public DirectionButtonListener(JTextPane textPane, DirectionResolver directionResolver, Board board, Direction direction) {
		super();
		this.textPane = textPane;
		this.directionResolver = directionResolver;
		this.board = board;
		this.direction = direction;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		getDirectionResolver().resolveDirection(direction);
		getTextPane().setText(getBoard().toString());
	}

	public JTextPane getTextPane() {
		return textPane;
	}

	public DirectionResolver getDirectionResolver() {
		return directionResolver;
	}

	public Board getBoard() {
		return board;
	}

}
