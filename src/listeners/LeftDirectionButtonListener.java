package listeners;

import board.Board;
import board.Direction;
import board.DirectionResolver;

import javax.swing.*;

public class LeftDirectionButtonListener extends DirectionButtonListener {

	public LeftDirectionButtonListener(JTextPane textPane, DirectionResolver directionResolver, Board board) {
		super(textPane, directionResolver, board, Direction.LEFT);
	}

}
