package window;
import java.awt.EventQueue;

import javax.swing.JFrame;

import board.Board;
import board.DirectionResolver;
import listeners.*;
import view.*;

public class SnakeFrame {
	private JFrame frame;
	private Board board;
	private DirectionResolver resolver;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SnakeFrame window = new SnakeFrame();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SnakeFrame() {
		initializeProperties();
		createFrame();
		initializeFrameChain();
	}

	/**
	 * Initialize the contents of the frame.
	 */

	private void initializeProperties(){
		board = new Board();
		resolver = new DirectionResolver(board);
	}

	private void createFrame(){
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.addKeyListener(new KeyboardListener());
	}

	private void initializeFrameChain() {

		ViewCreator chain = new JTextPaneCreator();

		chain
			.linkNext(new LeftJButtonCreator(resolver, board))
			.linkNext(new RightJButtonCreator(resolver, board))
			.linkNext(new DownJButtonCreator(resolver, board))
			.linkNext(new UpJButtonCreator(resolver, board));

		chain.invoke(frame);
	}
}
