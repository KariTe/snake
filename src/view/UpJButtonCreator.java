package view;

import board.Board;
import board.DirectionResolver;
import listeners.UpDirectionButtonListener;

import javax.swing.*;
import java.awt.*;

public class UpJButtonCreator extends JButtonCreator {

    private static final String UP = "\u02C4";
    private static final String FONT = "Arial";

    public UpJButtonCreator(DirectionResolver resolver, Board board) {
        super(resolver, board);
    }

    public void createViewComponent(JFrame jFrame) {
        JButton upButton = new JButton(UP);
        upButton.setFont(new Font(FONT, Font.PLAIN, 25));
        upButton.setBounds(168, 193, 117, 29);
        upButton.addActionListener(new UpDirectionButtonListener(getJTextPane(jFrame), resolver, board));
        jFrame.getContentPane().add(upButton);
    }
}
