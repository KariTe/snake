package view;

import board.Board;
import board.DirectionResolver;
import listeners.RightDirectionButtonListener;

import javax.swing.*;
import java.awt.*;

public class RightJButtonCreator extends JButtonCreator{

    private static final String RIGHT = "\u02C3";
    private static final String FONT = "Arial";

    public RightJButtonCreator(DirectionResolver resolver, Board board) {
        super(resolver, board);
    }

    @Override
    public void createViewComponent(JFrame jFrame) {
        JButton rightButton = new JButton(RIGHT);
		rightButton.setFont(new Font(FONT, Font.PLAIN, 50));
		rightButton.setBounds(286, 193, 117, 53);
		rightButton.addActionListener(new RightDirectionButtonListener(getJTextPane(jFrame), resolver, board));
		jFrame.getContentPane().add(rightButton);
    }
}
