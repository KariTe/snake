package view;

import board.Board;
import board.DirectionResolver;
import listeners.DownDirectionButtonListener;

import javax.swing.*;
import java.awt.*;

public class DownJButtonCreator extends JButtonCreator {
    private static final String DOWN = "\u02C5";
    private static final String FONT = "Arial";

    public DownJButtonCreator(DirectionResolver resolver, Board board) {
        super(resolver, board);
    }

    @Override
    public void createViewComponent(JFrame jFrame) {
        JButton downButton = new JButton(DOWN);
        downButton.setFont(new Font(FONT, Font.PLAIN, 25));
        downButton.setBounds(168, 217, 117, 29);
        downButton.addActionListener(new DownDirectionButtonListener(getJTextPane(jFrame), resolver, board));
        jFrame.getContentPane().add(downButton);
    }

}
