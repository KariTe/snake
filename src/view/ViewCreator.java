package view;

import javax.swing.*;
import java.util.Objects;

public abstract class ViewCreator {

    private ViewCreator next;
    // TODO pole do JFrame które bedzie przekazywane przez konstruktor(geterem z rodzica)
    protected abstract void createViewComponent(JFrame jFrame);


    public void invoke(JFrame jFrame) {
        createViewComponent(jFrame);
        invokeNext(jFrame);
    }

    private void invokeNext(JFrame jFrame){
        if (Objects.isNull(next)){
            return;
        }
        next.invoke(jFrame);
    }

    public ViewCreator linkNext(ViewCreator next){
        this.next = next;
        return this.next;
    }

    protected JTextPane getJTextPane(JFrame jFrame){
        return (JTextPane) jFrame.getContentPane().getComponent(0);

    }
}
