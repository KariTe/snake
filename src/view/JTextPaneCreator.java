package view;

import javax.swing.*;

public class JTextPaneCreator extends ViewCreator {

    @Override
    public void createViewComponent(JFrame jFrame) {
        JTextPane txtPane = new JTextPane();
        txtPane.setText("press any button to start");
        txtPane.setBounds(39, 6, 375, 174);
        txtPane.setAlignmentX(JTextPane.CENTER_ALIGNMENT);
        jFrame.getContentPane().add(txtPane);
    }
}
