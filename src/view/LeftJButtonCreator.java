package view;

import board.Board;
import board.DirectionResolver;
import listeners.LeftDirectionButtonListener;

import javax.swing.*;
import java.awt.*;

public class LeftJButtonCreator extends JButtonCreator {

    private static final String LEFT = "\u02C2";
    private static final String FONT = "Arial";

    public LeftJButtonCreator(DirectionResolver resolver, Board board) {
        super(resolver, board);
    }

    @Override
    public void createViewComponent(JFrame jFrame) {
        JButton leftButton = new JButton(LEFT);
        leftButton.setFont(new Font(FONT, Font.PLAIN, 50));
        leftButton.setBounds(54, 193, 117, 53);
        leftButton.addActionListener(new LeftDirectionButtonListener(getJTextPane(jFrame), resolver, board));
        jFrame.getContentPane().add(leftButton);
    }
}
