package view;

import board.Board;
import board.DirectionResolver;

public abstract class JButtonCreator extends ViewCreator{

    protected final DirectionResolver resolver;
    protected final Board board;

    public JButtonCreator(DirectionResolver resolver, Board board) {
        this.resolver = resolver;
        this.board = board;
    }
}
