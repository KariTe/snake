package board;


public class Point {

	private final int positionX;
	private final int positionY;
	
	
	public Point(int x, int y){
		this.positionX = x;
		this.positionY = y;
	}
	
	public int getX(){
		return positionX;
	}
	
	public int getY(){
		return positionY;
	}
	
}
