package board;


public class DirectionResolver {
	
		private final Board board;
		
		public DirectionResolver(Board board) {
			this.board = board;
		}

		public void resolveDirection(Direction direction) {
			board.move(direction);
		}
}
		
