package board;

public class Board {

	private static final int CLEAN_POSITION = 0;
	private static final int SET_POSITION = 1;
	private static final int WIDTH = 5;
	private static final int HEIGHT = 5;
	private static final int UP_BOUND = 0;
	private static final int DOWN_BOUND = 4;
	private static final int LEFT_BOUND = 0;
	private static final int RIGHT_BOUND = 4;
	private static final int STEP_FORWARD = 1;
	private static final String EMPTY_CELL = "\u2022";
	private static final String OCCUPIED_CELL = "\u00A4";

	private final int[][] snakeBoard = new int [WIDTH] [HEIGHT];
	
	private Point startingPosition = new Point (2,2);
	
	public Board(){
		snakeBoard[startingPosition.getX()][startingPosition.getY()] = 1;
	}
	
	public void move(Direction direction){
		if (direction == null) {
			return;
		}
		cleaningOldPosition();
		calculatingNewPosition(direction);
		settingNewPosition();
	}

	private void cleaningOldPosition(){
		snakeBoard[startingPosition.getX()][startingPosition.getY()] = CLEAN_POSITION;
	}

	private void calculatingNewPosition(Direction direction){

		Point newPosition;

		switch(direction) {
			case UP: newPosition = calcNewUpPosition(startingPosition);break;
			case DOWN: newPosition = calcNewDownPosition(startingPosition);break;
			case LEFT: newPosition = calcNewLeftPosition(startingPosition);break;
			case RIGHT: newPosition = calcNewRightPosition(startingPosition);break;
			default: newPosition = startingPosition;
		}
		startingPosition = newPosition;
	}

	private void settingNewPosition(){
		snakeBoard[startingPosition.getX()][startingPosition.getY()] = SET_POSITION;
	}

	private Point calcNewUpPosition(Point startingPosition) {
		int tmp = startingPosition.getY() - STEP_FORWARD;
		int y = (tmp < UP_BOUND) ? tmp + HEIGHT : tmp;
		return new Point(startingPosition.getX(), y);
	}

	
	private Point calcNewDownPosition(Point startingPosition) {
		int tmp = startingPosition.getY() + STEP_FORWARD;
		int y = (tmp > DOWN_BOUND) ? tmp - HEIGHT : tmp;
		return new Point(startingPosition.getX(), y);
	}

	
	private Point calcNewLeftPosition(Point startingPosition) {
		int tmp = startingPosition.getX() - STEP_FORWARD;
		int x = (tmp < LEFT_BOUND) ? tmp + WIDTH : tmp;
		return new Point(x, startingPosition.getY());
	}

	
	private Point calcNewRightPosition(Point startingPosition) {
		int tmp = startingPosition.getX() + STEP_FORWARD;
		int x = (tmp > RIGHT_BOUND) ? tmp - WIDTH : tmp;
		return new Point(x, startingPosition.getY());
	}
	
	public String toString() {
		String result = "";
		int tmp;
		 for(int i=0;i<snakeBoard.length;i++){
	         for(int j=0;j<snakeBoard[i].length;j++){
	        	 tmp = snakeBoard[j][i];
	        	 result = result + ((tmp == 0) ? EMPTY_CELL : OCCUPIED_CELL) + " ";
	         }
	         result += "\n";
		 }
		 return result;
	}
}